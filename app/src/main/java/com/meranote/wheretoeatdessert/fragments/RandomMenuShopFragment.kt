package com.meranote.wheretoeatdessert.fragments

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.GeoPoint
import com.meranote.wheretoeatdessert.R
import com.meranote.wheretoeatdessert.fragments.dialog.RandomMenuShopResultDialogFragment
import com.meranote.wheretoeatdessert.model.documents.ShopDocument
import com.meranote.wheretoeatdessert.repository.interfaces.ShopRepositoryInterface
import com.meranote.wheretoeatdessert.utilities.convertDpToPixel
import kotlinx.android.synthetic.main.fragment_random_menu_shop.*
import org.koin.android.ext.android.inject
import kotlin.math.roundToInt


class RandomMenuShopFragment : AbstractViewFragment(R.string.title_activity_random_menu_shop),
        OnMapReadyCallback, LocationListener,
        RandomMenuShopResultDialogFragment.OnButtonsInteraction {

    // Repository (via Koin inject)
    private val shopRepository: ShopRepositoryInterface by inject()

    // Location & Google Map
    private val DEFAULT_RADIUS: Double = 100000.0
    private lateinit var currentLocation: Location
    private lateinit var locationManager: LocationManager
    private var randomShopMarker: Marker? = null
    private var mMapFirstInit: Boolean = false
    private lateinit var mMap: GoogleMap

    // Dialogs
    private lateinit var latestResultDialog: RandomMenuShopResultDialogFragment

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_random_menu_shop, container, false)
    }

    @SuppressLint("MissingPermission")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // Get map and add listener
        (childFragmentManager.findFragmentById(R.id.random_map) as SupportMapFragment).getMapAsync(this)
        // Request current location
        locationManager = mainActivity!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates("fused", 400, 1000f, this)
        // Register button click listener
        randomBtn.setOnClickListener { onRandomBtnClick() }
        viewLastResultBtn.setOnClickListener { onViewLastResultBtnClick() }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        // Init to class property
        mMap = googleMap
        // Enable get my location button in map
        mMap.isMyLocationEnabled = true
    }

    override fun onLocationChanged(location: Location?) {
        if (!mMapFirstInit) {
            val latLng = LatLng(location!!.latitude, location.longitude)
            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15f)
            mMap.moveCamera(cameraUpdate)
            mMapFirstInit = true
        }
        if (location != null) {
            currentLocation = location
        }
    }
    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}
    override fun onProviderEnabled(provider: String?) {}
    override fun onProviderDisabled(provider: String?) {}

    private fun makeResultDialog(targetShop: ShopDocument, targetMenuIndex: Int) {
        latestResultDialog = RandomMenuShopResultDialogFragment.newInstance(targetShop, targetMenuIndex)
        latestResultDialog.setButtonsListener(this)
    }

    private fun onRandomBtnClick() {
        // Show overlay
        randomOverlayContainer.visibility = View.VISIBLE
        // Make handler for delayed after finishing search nearby and random
        val handle = Handler()
        // If already have marker in map, remove it
        if (randomShopMarker != null) {
            randomShopMarker!!.remove()
            randomShopMarker = null
        }
        // Find shop within radius from current device's location
        val myGeoPoint = GeoPoint(currentLocation.latitude, currentLocation.longitude)
        shopRepository.findAroundLocation(myGeoPoint, DEFAULT_RADIUS, {
            if (it.count() > 0) {
                // Random one shop
                val rShopIndex = Math.floor(Math.random() * it.count()).toInt()
                val rShop = it[rShopIndex]
                // Random one shop's menu
                val rMenuIndex = Math.floor(Math.random() * rShop.menu.count()).toInt()
                // Post delayed works
                handle.postDelayed({
                    // Add marker and move to marker position
                    val latLng = LatLng(rShop.location.latitude, rShop.location.longitude)
                    // Calculate camera to include both target and current location
                    // to both show in google map view
                    val builder = LatLngBounds.Builder().apply {
                        include(LatLng(myGeoPoint.latitude, myGeoPoint.longitude))
                        include(latLng)
                    }
                    val bounds = builder.build()
                    // Add marker and animate moving camera to bound
                    randomShopMarker = mMap.addMarker(MarkerOptions().position(latLng).title(rShop.name))
                    val camera = CameraUpdateFactory.newLatLngBounds(
                            bounds, convertDpToPixel(140f).roundToInt())
                    mMap.animateCamera(camera)
                    // Show Result Dialog
                    makeResultDialog(rShop, rMenuIndex)
                    latestResultDialog.show(fragmentManager, null)
                    // Show viewLastResultBtn
                    viewLastResultBtn.visibility = View.VISIBLE
                    // Hide overlay
                    randomOverlayContainer.visibility = View.GONE
                }, 1500)
            } else {
                Log.i("Shop.Nearby", "No shop nearby.")
                handle.postDelayed({
                    // Hide overlay
                    randomOverlayContainer.visibility = View.GONE
                    // TODO: Make Simple Dialog to show error
                }, 1500)
            }
        })
    }

    private fun onViewLastResultBtnClick() {
        if (this::latestResultDialog.isInitialized) {
            latestResultDialog.show(fragmentManager, null)
        }
    }

    override fun onReRandomBtnClick(dialog: Dialog) {
        // Close dialog first
        dialog.dismiss()
        // Then delegate to onRandomBtnClick
        onRandomBtnClick()
    }

    override fun onMakeRouteBtnClick(dialog: Dialog, lat: Double, long: Double) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(
                String.format("google.navigation:q=%f,%f", lat, long)))
        intent.`package` = "com.google.android.apps.maps"
        startActivity(intent)
    }
}
