package com.meranote.wheretoeatdessert.fragments.dialog

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.meranote.wheretoeatdessert.R
import com.meranote.wheretoeatdessert.model.documents.ShopDocument
import com.meranote.wheretoeatdessert.tasks.DownloadImageViewTask
import kotlinx.android.synthetic.main.dialog_random_menu_shop_result.view.*

private const val ARG_SHOP_PARAM = "target_shop"
private const val ARG_MENU_PARAM = "target_menu_index"

class RandomMenuShopResultDialogFragment : DialogFragment() {
    // Sent parameters
    private var targetShop: ShopDocument? = null
    private var targetMenuIndex: Int? = null

    // Listener for reRandomBtn
    private lateinit var buttonsListener: OnButtonsInteraction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            targetShop = it.getSerializable(ARG_SHOP_PARAM) as ShopDocument
            targetMenuIndex = it.getInt(ARG_MENU_PARAM)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the Builder class for convenient dialog construction
        val builder = AlertDialog.Builder(activity!!)
        // Get the layout inflater
        val inflater = activity!!.layoutInflater
        // Create view
        val targetMenu = targetShop!!.menu[targetMenuIndex!!]
        val view = inflater.inflate(R.layout.dialog_random_menu_shop_result, null)
        // Set view's components properties
        // Menu Picture
        DownloadImageViewTask(view.menuPictureImageView).execute(targetMenu.picture)
        // Menu Name
        view.menuNameTextView.text = targetMenu.name
        view.shopNameTextView.text = String.format("@%s", targetShop!!.name)
        view.menuDescriptionTextView.text = targetMenu.description
        view.menuPriceTextView.text = String.format("Price: %.2f Baht", targetMenu.price)
        // Set button interaction
        view.viewMapBtn.setOnClickListener {
            dialog.dismiss()
        }
        view.makeRouteBtn.setOnClickListener {
            if (this::buttonsListener.isInitialized) {
                buttonsListener.onMakeRouteBtnClick(
                        this@RandomMenuShopResultDialogFragment.dialog,
                        targetShop!!.location.latitude, targetShop!!.location.longitude
                )
            }
        }
        view.reRandomBtn.setOnClickListener {
            if (this::buttonsListener.isInitialized) {
                buttonsListener.onReRandomBtnClick(this@RandomMenuShopResultDialogFragment.dialog)
            }
        }
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(view)
        // Then create dialog
        return builder.create()
    }

    fun setButtonsListener(listener: OnButtonsInteraction) {
        buttonsListener = listener
    }

    interface OnButtonsInteraction {
        fun onReRandomBtnClick(dialog: Dialog)
        fun onMakeRouteBtnClick(dialog: Dialog, lat: Double, long: Double)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         */
        @JvmStatic
        fun newInstance(targetShop: ShopDocument, targetMenuIndex: Int) =
                RandomMenuShopResultDialogFragment().apply {
                    arguments = Bundle().apply {
                        putSerializable(ARG_SHOP_PARAM, targetShop)
                        putInt(ARG_MENU_PARAM, targetMenuIndex)
                    }
                }
    }
}
