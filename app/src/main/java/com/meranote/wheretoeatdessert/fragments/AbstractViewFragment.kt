package com.meranote.wheretoeatdessert.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import com.meranote.wheretoeatdessert.activities.MainActivity

abstract class AbstractViewFragment(
        val title: kotlin.Any
): Fragment() {

    init {
        if (!(title is String || title is Int)) {
            throw Exception("Title must be an String or Int type.")
        }
    }

    protected var mainActivity: MainActivity? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is MainActivity) {
            mainActivity = context
        } else {
            throw RuntimeException(context.toString() + " must be the MainActivity")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // Set action bar title
        when (title) {
            is String -> (context as MainActivity).setActionBarTitle(title)
            is Int -> (context as MainActivity).setActionBarTitle(title)
        }
    }

    override fun onDetach() {
        super.onDetach()
        mainActivity = null
    }

}
