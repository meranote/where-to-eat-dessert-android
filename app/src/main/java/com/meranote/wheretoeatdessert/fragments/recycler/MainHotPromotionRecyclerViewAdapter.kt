package com.meranote.wheretoeatdessert.fragments.recycler

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.meranote.wheretoeatdessert.R
import com.meranote.wheretoeatdessert.model.documents.HotPromotionDocument
import com.meranote.wheretoeatdessert.tasks.DownloadImageViewTask
import kotlinx.android.synthetic.main.rv_main_hotpromotion.view.*

class MainHotPromotionRecyclerViewAdapter(
        private var models: List<HotPromotionDocument>,
        private val listener: OnListInteractionListener?)
    : RecyclerView.Adapter<MainHotPromotionRecyclerViewAdapter.ViewHolder>() {

    private val onViewClickListener: View.OnClickListener

    init {
        onViewClickListener = View.OnClickListener {
            listener?.onHotPromotionItemClick(it.tag as HotPromotionDocument)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.rv_main_hotpromotion, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = models[position]
        DownloadImageViewTask(holder.imageView).execute(item.picture)
        holder.textView.text = item.title

        with(holder.view) {
            tag = item
            setOnClickListener(onViewClickListener)
        }
    }

    override fun getItemCount(): Int = models.size

    fun setModels(models: List<HotPromotionDocument>) {
        this.models = models
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val imageView: ImageView = view.rHotPromotionImageView
        val textView: TextView = view.rHotPromotionTitle
    }

    interface OnListInteractionListener {
        fun onHotPromotionItemClick(item: HotPromotionDocument?)
    }
}
