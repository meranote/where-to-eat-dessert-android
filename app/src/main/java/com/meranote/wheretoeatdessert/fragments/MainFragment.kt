package com.meranote.wheretoeatdessert.fragments

import android.net.Uri
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.meranote.wheretoeatdessert.R
import com.meranote.wheretoeatdessert.fragments.recycler.MainHotPromotionRecyclerViewAdapter
import com.meranote.wheretoeatdessert.model.documents.HotPromotionDocument
import com.meranote.wheretoeatdessert.model.views.home.HighlightModel
import com.meranote.wheretoeatdessert.model.views.home.RecommendShopModel
import com.meranote.wheretoeatdessert.repository.interfaces.HotPromotionRepositoryInterface
import com.meranote.wheretoeatdessert.tasks.DownloadImageViewTask
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.fragment_main.view.*
import org.koin.android.ext.android.inject

class MainFragment: AbstractViewFragment(R.string.title_activity_main),
        MainHotPromotionRecyclerViewAdapter.OnListInteractionListener {

    // Repositories (via Koin Inject)
    private val hotPromotionRepository: HotPromotionRepositoryInterface by inject()

    // Models
    private var hotPromotions: List<HotPromotionDocument> = arrayListOf()
    private var highlight: HighlightModel? = null
    private var recommendShop: RecommendShopModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main, container, false)

        // Set the adapter
        if (view.mainHotPromotionList is RecyclerView) {
            with(view.mainHotPromotionList) {
                // Setup layout and adapter
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                adapter = MainHotPromotionRecyclerViewAdapter(hotPromotions, this@MainFragment)
                // Fetch hot promotion data
                hotPromotionRepository.all({
                    (adapter as MainHotPromotionRecyclerViewAdapter).setModels(it)
                    adapter.notifyDataSetChanged()
                })
            }
        }

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // Add Jumbos click listener
        mainHighlight.setOnClickListener { onJumboHighlightClick() }
        mainRecommendShop.setOnClickListener { onJumboRecommendShopClick() }
        // Setup Mocking
        setupMocking()
    }

    private fun changeHighlightModel(model: HighlightModel) {
        // Update view
        mainHighlightTitle.text = model.title
        mainHighlightSubTitle.text = model.subTitle
        DownloadImageViewTask(mainHighlightImageView).execute(model.picture)
        // Update model
        highlight = model
    }

    private fun changeRecommendShopModel(model: RecommendShopModel) {
        // Update view
        mainRecommendShopTitle.text = model.title
        mainRecommendShopSubTitle.text = model.subTitle
        DownloadImageViewTask(mainRecommendShopImageView).execute(model.picture)
        // Update model
        recommendShop = model
    }

    override fun onHotPromotionItemClick(item: HotPromotionDocument?) {
        Log.i("HotProItem.Click", String.format("Hot promotion item clicked; id=%s", item?.__selfTarget))
    }

    private fun onJumboHighlightClick() {
        Log.i("J_Highlight.Click", this.highlight?.target.toString())
    }

    private fun onJumboRecommendShopClick() {
        Log.i("J_RecommendShop.Click", this.recommendShop?.target.toString())
    }

    private fun setupMocking() {
        changeHighlightModel(HighlightModel(
                "Turtle Brownie Ice Cream",
                "Meranote Bakery",
                Uri.parse("https://images-gmi-pmc.edge-generalmills.com/34c0ac81-656b-45f1-b0b9-6eff351f2dc9.jpg"),
                Uri.parse("http://wheretoeatdessert.meranote.com/shops/1/menus/1124dsaews3")
        ))
        changeRecommendShopModel(RecommendShopModel(
                "Meranote Bakery",
                "The best dessert experience",
                Uri.parse("https://www.rizeindex.com/wp-content/uploads/2016/02/lafayetta-2-1.jpg"),
                Uri.parse("http://wheretoeatdessert.meranote.com/shops/1")
        ))
    }
}
