package com.meranote.wheretoeatdessert.net

interface Result<TResult> {
    fun onSuccessful(result: TResult)
    fun onFailure(e: Exception?)
}