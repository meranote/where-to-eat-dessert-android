package com.meranote.wheretoeatdessert.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.meranote.wheretoeatdessert.R
import com.meranote.wheretoeatdessert.fragments.MainFragment
import com.meranote.wheretoeatdessert.fragments.RandomMenuShopFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var currentView: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setup Navigation Drawer
        setupNavigationDrawer()
        // Change view fragment to MainFragment (home page)
        changeViewFragment(MainFragment())
    }

    private fun setupNavigationDrawer() {
        // Set Toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu)
        // Don't tint icon color
        nav_view.itemIconTintList = null
        // On Navigation item selected
        nav_view.setNavigationItemSelectedListener { onNavigationItemSelected(it) }
        // On random button clicked
        randomBtn.setOnClickListener { onRandomBtnClick(it) }
    }

    fun setActionBarTitle(title: String) {
        supportActionBar!!.title = title
    }

    fun setActionBarTitle(resource: Int) {
        setActionBarTitle(resources.getString(resource))
    }

    fun changeViewFragment(fragment: Fragment) {
        // Change fragment
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, fragment)
//                .addToBackStack(null)
                .commit()
        // Set current view to new changed fragment
        currentView = fragment::class.java.name
    }

    private fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        // close drawer when item is tapped
        drawer_layout.closeDrawers()

        // Select target class of item that have been selected
        val target: Class<*>? = when(menuItem.itemId) {
            R.id.nav_item_home -> MainFragment::class.java
            R.id.nav_item_favorite -> null
            else -> null
        }

        // Change view to target if its not a current view
        return if (target != null && target.name !== currentView) {
            val fragment = target.newInstance() as Fragment
            changeViewFragment(fragment)
            menuItem.isChecked = true
            true
        } else {
            false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onRandomBtnClick(view: View) {
        // Close drawer
        drawer_layout.closeDrawers()
        // If current view is currently RandomMenuShopFragment, returned
        if (RandomMenuShopFragment::class.java.name === currentView) return
        // Uncheck all navigation items
        for (i: Int in 0..(nav_view.menu.size() - 1)) {
            nav_view.menu.getItem(i).isChecked = false
        }
        // Create fragment and give it an argument specifying the article it should show
        val newFragment = RandomMenuShopFragment()
        // Change fragment
        changeViewFragment(newFragment)
    }

    fun onViewInteraction(string: String) {
        Toast.makeText(this@MainActivity, string, Toast.LENGTH_SHORT).show()
    }
}
