package com.meranote.wheretoeatdessert.activities

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.Manifest.permission.INTERNET
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.meranote.wheretoeatdessert.R
import com.meranote.wheretoeatdessert.utilities.hasPermissions

class SplashActivity : AppCompatActivity() {

    private lateinit var postPermissionRequestRunner: Runnable
    private var handle = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        postPermissionRequestRunner = Runnable {
            startActivity(Intent(this@SplashActivity, MainActivity::class.java))
            finish()
        }

        val permissions = arrayOf(ACCESS_FINE_LOCATION, INTERNET)
        if (!hasPermissions(this, permissions)) {
            ActivityCompat.requestPermissions(this, permissions, 1001)
        } else {
            handle.postDelayed(postPermissionRequestRunner, 1500)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        val permissionsMap = HashMap<String, Int>()
        for ((index, permission) in permissions.withIndex()) {
            permissionsMap[permission] = grantResults[index]
        }

        if (permissionsMap[ACCESS_FINE_LOCATION] != 0) {
            Toast.makeText(this, "Location permissions are a must", Toast.LENGTH_SHORT).show()
            finish()
        } else {
            handle.postDelayed(postPermissionRequestRunner, 1500)
        }
    }

}
