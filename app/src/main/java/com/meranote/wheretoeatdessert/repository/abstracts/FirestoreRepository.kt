package com.meranote.wheretoeatdessert.repository.abstracts

import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import com.meranote.wheretoeatdessert.model.documents.Document
import com.meranote.wheretoeatdessert.net.Result
import com.meranote.wheretoeatdessert.repository.interfaces.QueryCriteria
import com.meranote.wheretoeatdessert.repository.interfaces.QueryCriteriaOperator
import com.meranote.wheretoeatdessert.repository.interfaces.Repository

abstract class FirestoreRepository<T: Document>(
        protected val firestore: FirebaseFirestore,
        protected val collection: String
) : Repository<T> {
    override fun findOne(id: String, callback: Result<T?>) {
        firestore.collection(collection).document(id).get()
                .addOnCompleteListener { onSingleDocumentQueryComplete(it, callback) }
    }

    override fun findBy(criteria: List<QueryCriteria>, callback: Result<List<T>>) {
        // Make queries by given criteria
        if (criteria.isEmpty()) throw Exception("No criteria specified.")
        var queries: Query? = null
        criteria.forEach {
            queries = if (queries == null) {
                firestore.collection(collection).addQuery(it)
            } else {
                queries!!.addQuery(it)
            }
        }
        // Execute query
        queries!!.get()
                .addOnCompleteListener { onMultipleDocumentsQueryComplete(it, callback) }
    }

    override fun all(callback: Result<List<T>>) {
        firestore.collection(collection).get()
                .addOnCompleteListener { onMultipleDocumentsQueryComplete(it, callback) }
    }

    override fun count(callback: Result<Int>) {
        firestore.collection(collection).get()
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        callback.onSuccessful(it.result.size())
                    } else {
                        callback.onFailure(it.exception)
                    }
                }

    }

    abstract fun castObject(snapshot: DocumentSnapshot): T

    private fun onSingleDocumentQueryComplete(
            query: Task<DocumentSnapshot>, callback: Result<T?>
    ) {
        if (query.isSuccessful) {
            val doc: DocumentSnapshot = query.result
            if (doc.exists()) {
                castObject(doc)
            }
        } else {
            callback.onFailure(query.exception)
        }
    }

    private fun onMultipleDocumentsQueryComplete(
            query: Task<QuerySnapshot>, callback: Result<List<T>>) {
        if (query.isSuccessful) {
            var documents: List<T> = listOf()
            if (query.result.size() > 0) {
                documents = query.result.map {
                    castObject(it)
                }
            }
            callback.onSuccessful(documents)
        } else {
            callback.onFailure(query.exception)
        }
    }
}

/**
 * Add method to Firestore Query class to support Our QueryCriteria.
 */
fun Query.addQuery(criteria: QueryCriteria): Query {
    return criteria.run {
        val query = this@addQuery
        when (operator) {
            QueryCriteriaOperator.EQUAL -> query.whereEqualTo(key, value)
            QueryCriteriaOperator.GREATER_THAN -> query.whereGreaterThan(key, value)
            QueryCriteriaOperator.GREATER_THAN_OR_EQUAL -> query.whereGreaterThanOrEqualTo(key, value)
            QueryCriteriaOperator.LESS_THAN -> query.whereLessThan(key, value)
            QueryCriteriaOperator.LESS_THAN_OR_EQUAL -> query.whereLessThanOrEqualTo(key, value)
            else -> throw Exception("Operator not supported.")
        }
    }
}
