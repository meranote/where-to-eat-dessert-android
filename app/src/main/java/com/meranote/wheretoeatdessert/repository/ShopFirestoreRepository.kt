package com.meranote.wheretoeatdessert.repository

import android.net.Uri
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.GeoPoint
import com.meranote.wheretoeatdessert.model.documents.ShopDocument
import com.meranote.wheretoeatdessert.model.fields.MenuShopField
import com.meranote.wheretoeatdessert.model.fields.ServiceTimeField
import com.meranote.wheretoeatdessert.repository.abstracts.FirestoreRepository
import com.meranote.wheretoeatdessert.repository.interfaces.QueryCriteria
import com.meranote.wheretoeatdessert.repository.interfaces.QueryCriteriaOperator.GREATER_THAN_OR_EQUAL
import com.meranote.wheretoeatdessert.repository.interfaces.QueryCriteriaOperator.LESS_THAN_OR_EQUAL
import com.meranote.wheretoeatdessert.repository.interfaces.ShopRepositoryInterface
import com.meranote.wheretoeatdessert.utilities.LocationUtility

class ShopFirestoreRepository(firestore: FirebaseFirestore)
    : FirestoreRepository<ShopDocument>(
        firestore,
        "shops"
), ShopRepositoryInterface {
    override fun castObject(snapshot: DocumentSnapshot): ShopDocument {
        // Cast Shop's menus
        val menus = mutableListOf<MenuShopField>()
        (snapshot.get("menu") as List<*>).forEach {
            menus.add((it as Map<*, *>).run {
                // Cast price
                val price: Double = try {
                    (get("price") as Long).toDouble()
                } catch (ignored: Exception) {
                    get("price") as Double
                }
                MenuShopField(
                        get("name") as String,
                        get("description") as String,
                        price,
                        Uri.parse(get("picture") as String),
                        get("of") as String
                )
            })
        }
        // Cast Service time
        val serviceTime = hashMapOf<String, ServiceTimeField?>()
        (snapshot.get("service_time") as Map<*, *>).forEach {
            if (it.value != null) {
                serviceTime[it.key as String] = (it.value as Map<*, *>).run {
                    ServiceTimeField(
                            get("opened") as String,
                            get("closed") as String
                    )
                }
            } else {
                serviceTime[it.key as String] = null
            }
        }
        // Cast to ShopDocument
        return ShopDocument(
                snapshot.id,
                snapshot.get("name") as String,
                snapshot.get("description") as String,
                snapshot.get("address") as String,
                snapshot.get("location") as GeoPoint,
                snapshot.get("telephone") as String,
                snapshot.get("type") as String,
                snapshot.get("tags") as List<String>,
                serviceTime,
                Uri.parse(snapshot.get("profile_picture") as String),
                menus
        )
    }

    override fun findAroundLocation(
            location: GeoPoint,
            radius: Double,
            callback: (result: List<ShopDocument>) -> Unit)
    {
        // Calculate corner bounds of rectangle (within area to search)
        val cornerBounds =
                LocationUtility.makeRectangleAreaUpperLowerGeoPoint(location, radius / 1000)
        // Find by compare within area and bind callback
        findBy(listOf(
                QueryCriteria("location", cornerBounds[0], LESS_THAN_OR_EQUAL),
                QueryCriteria("location", cornerBounds[1], GREATER_THAN_OR_EQUAL)
        ), callback)
    }
}