package com.meranote.wheretoeatdessert.repository.interfaces

import com.google.firebase.firestore.GeoPoint
import com.meranote.wheretoeatdessert.model.documents.ShopDocument

interface ShopRepositoryInterface: Repository<ShopDocument> {
    fun findAroundLocation(location: GeoPoint, radius: Double,
                           callback: (result: List<ShopDocument>) -> Unit)
}
