package com.meranote.wheretoeatdessert.repository.dummies

import android.net.Uri
import com.meranote.wheretoeatdessert.model.documents.HotPromotionDocument
import com.meranote.wheretoeatdessert.net.Result
import com.meranote.wheretoeatdessert.repository.interfaces.HotPromotionRepositoryInterface
import com.meranote.wheretoeatdessert.repository.interfaces.QueryCriteria
import java.util.*

val dummies: List<HotPromotionDocument> = arrayListOf(
        HotPromotionDocument(
                null,
                "80% Off dessert",
                "Start 80% Off dessert now!",
                "Start 80% Off dessert now!, Long Description",
                Date(),
                Date(),
                Uri.parse("https://i.pinimg.com/originals/58/d2/2c/58d22cbc33f099240a3ea7be6b969a55.jpg"),
                Uri.parse("http://wheretoeatdessert.meranote.com/promotion/1")
        ),
        HotPromotionDocument(
                null,
                "80% Off dessert",
                "Start 80% Off dessert now!",
                "Start 80% Off dessert now!, Long Description",
                Date(),
                Date(),
                Uri.parse("https://i.pinimg.com/originals/58/d2/2c/58d22cbc33f099240a3ea7be6b969a55.jpg"),
                Uri.parse("http://wheretoeatdessert.meranote.com/promotion/1")
        ),
        HotPromotionDocument(
                null,
                "80% Off dessert",
                "Start 80% Off dessert now!",
                "Start 80% Off dessert now!, Long Description",
                Date(),
                Date(),
                Uri.parse("https://i.pinimg.com/originals/58/d2/2c/58d22cbc33f099240a3ea7be6b969a55.jpg"),
                Uri.parse("http://wheretoeatdessert.meranote.com/promotion/1")
        )
)

class HotPromotionDummyRepository: HotPromotionRepositoryInterface {
    override fun count(callback: Result<Int>) {
        callback.onSuccessful(dummies.size)
    }

    override fun findBy(criteria: List<QueryCriteria>, callback: Result<List<HotPromotionDocument>>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun findOne(id: String, callback: Result<HotPromotionDocument?>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun all(callback: Result<List<HotPromotionDocument>>) {
        callback.onSuccessful(dummies)
    }
}