package com.meranote.wheretoeatdessert.repository

import android.net.Uri
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.meranote.wheretoeatdessert.model.documents.HotPromotionDocument
import com.meranote.wheretoeatdessert.repository.abstracts.FirestoreRepository
import com.meranote.wheretoeatdessert.repository.interfaces.HotPromotionRepositoryInterface
import java.util.*

class HotPromotionFirestoreRepository(firestore: FirebaseFirestore)
    : FirestoreRepository<HotPromotionDocument>(
        firestore,
        "hot_promotions"
), HotPromotionRepositoryInterface {
    override fun castObject(snapshot: DocumentSnapshot): HotPromotionDocument {
        return HotPromotionDocument(
                snapshot.id,
                snapshot.get("title") as String,
                snapshot.get("short_description") as String,
                snapshot.get("description") as String,
                snapshot.get("start_date") as Date,
                snapshot.get("end_date") as Date,
                Uri.parse(snapshot.get("picture") as String),
                Uri.parse(snapshot.get("target") as String)
        )
    }
}
