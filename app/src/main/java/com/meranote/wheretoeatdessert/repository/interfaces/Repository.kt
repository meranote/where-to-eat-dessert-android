package com.meranote.wheretoeatdessert.repository.interfaces

import android.util.Log
import com.meranote.wheretoeatdessert.net.Result

interface Repository<T> {
    /** Get all data. */
    fun all(callback: Result<List<T>>)
    /** Total count (size) of data in repository */
    fun count(callback: Result<Int>)
    /** Get data with criteria specified. */
    fun findBy(criteria: List<QueryCriteria>, callback: Result<List<T>>)
    /** Get data by id. */
    fun findOne(id: String, callback: Result<T?>)

    // region Overload for inline function
    /** Get all data. */
    fun all(callback: (result: List<T>) -> Unit) {
        all(object: Result<List<T>> {
            override fun onSuccessful(result: List<T>) {
                callback(result)
            }
            override fun onFailure(e: Exception?) {
                Log.e("Repository", "On query 'all()' error.", e)
            }
        })
    }
    /** Total count (size) of data in repository */
    fun count(callback:(result: Int) -> Unit) {
        count(object: Result<Int> {
            override fun onSuccessful(result: Int) {
                callback(result)
            }
            override fun onFailure(e: Exception?) {
                Log.e("Repository", "On query 'all()' error.", e)
            }
        })
    }
    /** Get data with criteria specified. */
    fun findBy(criteria: List<QueryCriteria>, callback: (result: List<T>) -> Unit) {
        findBy(criteria, object: Result<List<T>> {
            override fun onSuccessful(result: List<T>) {
                callback(result)
            }
            override fun onFailure(e: Exception?) {
                Log.e("Repository", "On query 'all()' error.", e)
            }
        })
    }
    /** Get data by id. */
    fun findOne(id: String, callback: (result: T?) -> Unit) {
        findOne(id, object: Result<T?> {
            override fun onSuccessful(result: T?) {
                callback(result)
            }
            override fun onFailure(e: Exception?) {
                Log.e("Repository", "On query 'all()' error.", e)
            }
        })
    }
    // endregion
}
