package com.meranote.wheretoeatdessert.repository.interfaces

data class QueryCriteria(
        val key: String,
        val value: Any,
        val operator: QueryCriteriaOperator = QueryCriteriaOperator.EQUAL
)

enum class QueryCriteriaOperator {
    EQUAL,
    NOT_EQUAL,
    GREATER_THAN,
    GREATER_THAN_OR_EQUAL,
    LESS_THAN,
    LESS_THAN_OR_EQUAL,
}
