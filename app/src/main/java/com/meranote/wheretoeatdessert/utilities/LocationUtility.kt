package com.meranote.wheretoeatdessert.utilities

import com.google.firebase.firestore.GeoPoint

// Semi-axes of WGS-84 geoidal reference
private val WGS84_a = 6378137.0  // Major semiaxis [m]
private val WGS84_b = 6356752.3  // Minor semiaxis [m]

/** Earth radius at a given latitude, according to the WGS-84 ellipsoid */
private fun WGS84EarthRadius(lat: Double): Double {
    // http://en.wikipedia.org/wiki/Earth_radius
    val An = WGS84_a * WGS84_a * Math.cos(lat)
    val Bn = WGS84_b * WGS84_b * Math.sin(lat)
    val Ad = WGS84_a * Math.cos(lat)
    val Bd = WGS84_b * Math.sin(lat)
    return Math.sqrt( (An*An + Bn*Bn)/(Ad*Ad + Bd*Bd) )
}

abstract class LocationUtility {
    companion object {
        fun moveGeoPoint(base: GeoPoint,
                                         moveLatInKm: Double, moveLongInKm: Double): GeoPoint {
            val lat = Math.toRadians(base.latitude)
            val long = Math.toRadians(base.longitude)
            // Radius of Earth at given latitude
            val radius = WGS84EarthRadius(lat)
            // Radius of the parallel at given latitude
            val pradius = radius * Math.cos(lat)
            // Move latitude
            val resultLat = lat + ((1000 * moveLatInKm) / radius)
            // Move longitude
            val resultLong = long + ((1000 * moveLongInKm) / pradius)
            // Return new GeoPoint
            return GeoPoint(Math.toDegrees(resultLat), Math.toDegrees(resultLong))
        }

        fun moveLatitude(base: GeoPoint, rangeInKm: Double): GeoPoint {
            return moveGeoPoint(base, rangeInKm, 0.0)
        }

        fun moveLongitude(base: GeoPoint, rangeInKm: Double): GeoPoint {
            return moveGeoPoint(base, 0.0, rangeInKm)
        }

        fun makeRectangleAreaUpperLowerGeoPoint(
                base: GeoPoint, radiusInKm: Double): List<GeoPoint> {
            return listOf(
                    moveGeoPoint(base, radiusInKm, radiusInKm),     // Upper-Right Bound
                    moveGeoPoint(base, -radiusInKm, -radiusInKm)    // Lower-Left Bound
            )
        }

        fun makeRectangleAreaCornerGeoPoint(
                base: GeoPoint, radiusInKm: Double): List<GeoPoint> {
            return listOf(
                    moveGeoPoint(base, radiusInKm, 0.0),    // Upper Bound
                    moveGeoPoint(base, -radiusInKm, 0.0),   // Lower Bound
                    moveGeoPoint(base, 0.0, radiusInKm),     // Right Bound
                    moveGeoPoint(base, 0.0, -radiusInKm)     // Left Bound
            )
        }
    }
}


