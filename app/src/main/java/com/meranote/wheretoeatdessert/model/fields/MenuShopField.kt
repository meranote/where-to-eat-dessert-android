package com.meranote.wheretoeatdessert.model.fields

import android.net.Uri

data class MenuShopField(
        val name: String,
        val description: String,
        val price: Double,
        val picture: Uri,
        val of: String
)
