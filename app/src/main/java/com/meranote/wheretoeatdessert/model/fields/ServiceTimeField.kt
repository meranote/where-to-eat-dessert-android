package com.meranote.wheretoeatdessert.model.fields

data class ServiceTimeField(
        val opened: String,
        val closed: String
)