package com.meranote.wheretoeatdessert.model.views

import android.net.Uri

abstract class AbstractJumboViewModel {
    abstract val title: String
    abstract val subTitle: String
    abstract val picture: Uri
    abstract val target: Uri
}
