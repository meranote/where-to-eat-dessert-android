package com.meranote.wheretoeatdessert.model.documents

import android.net.Uri

data class MenuDocument(
        override val __id: String? = null,
        val name: String,
        val description: String,
        val tags: List<String>,
        val picture: Uri,
        val type: String
): Document()
