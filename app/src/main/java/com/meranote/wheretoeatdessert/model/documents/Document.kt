package com.meranote.wheretoeatdessert.model.documents

import java.io.Serializable

abstract class Document : Serializable {
    abstract val __id: String?
}
