package com.meranote.wheretoeatdessert.model.documents

import android.net.Uri
import com.google.firebase.firestore.GeoPoint
import com.meranote.wheretoeatdessert.model.fields.MenuShopField
import com.meranote.wheretoeatdessert.model.fields.ServiceTimeField

data class ShopDocument(
        override val __id: String? = null,
        val name: String,
        val description: String,
        val address: String,
        val location: GeoPoint,
        val telephone: String,
        val type: String,
        val tags: List<String>,
        val serviceTime: Map<String, ServiceTimeField?>,
        val profilePicture: Uri,
        val menu: List<MenuShopField>
): Document()
