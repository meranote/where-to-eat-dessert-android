package com.meranote.wheretoeatdessert.model.views.home

import android.net.Uri
import com.meranote.wheretoeatdessert.model.views.AbstractJumboViewModel

data class RecommendShopModel(
        override val title: String,
        override val subTitle: String,
        override val picture: Uri,
        override val target: Uri
): AbstractJumboViewModel()