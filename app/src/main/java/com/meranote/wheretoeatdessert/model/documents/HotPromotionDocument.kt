package com.meranote.wheretoeatdessert.model.documents

import android.net.Uri
import java.util.*

data class HotPromotionDocument(
        override val __id: String? = null,
        val title: String,
        val shortDescription: String,
        val description: String,
        val startDate: Date,
        val endDate: Date,
        val picture: Uri,
        val target: Uri
): Document() {
    val __selfTarget: Uri = Uri.parse(String.format(
            "http://wheretoeatdessert.meranote.com/promotion/%s",
            __id
    ))
}
