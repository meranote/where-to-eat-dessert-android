package com.meranote.wheretoeatdessert

import com.google.firebase.firestore.FirebaseFirestore
import com.meranote.wheretoeatdessert.repository.HotPromotionFirestoreRepository
import com.meranote.wheretoeatdessert.repository.ShopFirestoreRepository
import com.meranote.wheretoeatdessert.repository.interfaces.HotPromotionRepositoryInterface
import com.meranote.wheretoeatdessert.repository.interfaces.ShopRepositoryInterface
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext

val mainDIModule: Module = applicationContext {
    bean { FirebaseFirestore.getInstance() }
    bean { ShopFirestoreRepository(get()) as ShopRepositoryInterface }
    bean { HotPromotionFirestoreRepository(get()) as HotPromotionRepositoryInterface }
}
