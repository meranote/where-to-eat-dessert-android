package com.meranote.wheretoeatdessert.tasks

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.util.Log
import android.widget.ImageView
import java.net.URL

@SuppressLint("StaticFieldLeak")
class DownloadImageViewTask(
        private val view: ImageView
): AsyncTask<Uri, Void, Bitmap>() {

    override fun doInBackground(vararg params: Uri?): Bitmap {
        val url = params[0].toString()
        var bmp: Bitmap? = null

        try {
            bmp = BitmapFactory.decodeStream(URL(url).openConnection().getInputStream())
        } catch (e: Exception) {
            Log.e("DL_IMG_TASK.Error", e.message)
            e.printStackTrace()
        }

        return bmp!!
    }

    override fun onPostExecute(result: Bitmap?) {
        view.setImageBitmap(result)
    }

}