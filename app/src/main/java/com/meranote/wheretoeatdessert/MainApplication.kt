package com.meranote.wheretoeatdessert

import android.app.Application
import org.koin.android.ext.android.startKoin

class MainApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin(this, listOf(mainDIModule))
    }
}